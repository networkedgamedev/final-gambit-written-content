using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BattleActionInfo
{
    public static string GetInfoForID(int id)
    {
        string info = "";

        #region Attacks & Heals

        if (id == BattleActionID.Attack)
            info = GetDamageText(true, false, false, BattleActionParams.AttackMod);

        else if (id == BattleActionID.MagicMissile)
            info = GetDamageText(true, true, false, BattleActionParams.MagicMissileMod);
        else if (id == BattleActionID.FlameStrike)
            info = GetDamageText(true, true, false, BattleActionParams.FlameStrikeMod);

        else if (id == BattleActionID.Fireball)
            info = GetDamageText(true, true, true, BattleActionParams.FireballMod);
        else if (id == BattleActionID.Meteor)
            info = GetDamageText(true, true, true, BattleActionParams.MeteorMod);

        else if (id == BattleActionID.CureLight)
            info = GetDamageText(false, true, false, BattleActionParams.CureLightMod);
        else if (id == BattleActionID.CureSerious)
            info = GetDamageText(false, true, false, BattleActionParams.CureSeriousMod);

        else if (id == BattleActionID.MassHeal)
            info = GetDamageText(false, true, true, BattleActionParams.MassHealMod);

        else if (id == BattleActionID.FlurryOfBlows)
            info = GetDamageText(true, false, true, BattleActionParams.FlurryOfBlowsMod);


        else if (id == BattleActionID.Resurrection)
            info = "Revives and fully heals target ally.";
        else if (id == BattleActionID.Revive)
            info = "Revives and fully heals target ally.";

        else if (id == BattleActionID.Chakra)
            info = "Replenishes " + BattleActionParams.ChakraMod + " * Intelligence mana of target ally.  Performing Chakra reduces max mana by " + BattleActionParams.ChakraMaxMPReduction + ".  Performing Chakra requires a max mana of " + BattleActionParams.ChakraMaxMPReduction + " or greater.";

        else if (id == BattleActionID.Potion)
            info = "Fully heals target ally.";
        else if (id == BattleActionID.Ether)
            info = "Fully restores target ally's mana.";
        else if (id == BattleActionID.Elixir)
            info = "Fully heals and replenishes the mana of target ally.";
        else if (id == BattleActionID.MegaElixir)
            info = "Fully heals and replenishes the mana of all ally characters.";

        else if (id == BattleActionID.QuickHit)
            info = GetDamageText(true, false, false, BattleActionParams.QucikHitDamageMod) + "  The character performing Quick Hit has their initiative replenished by " + (BattleActionParams.QucikHitInitReplenish * 100) + "%.";
        //"Deals physical damage and replenishes the actor's initiative bar by " + (BattleActionParams.QucikHitInitReplenish * 100) + "%.  Where damage = [Actor's Attack] * " + BattleActionParams.QucikHitDamageMod + " * (1 - [Target's Defense] / 1000).";
        else if (id == BattleActionID.QuickHeal)
            info = GetDamageText(false, true, false, BattleActionParams.QuickHealMod) + "  The character performing Quick Heal has their initiative replenished by " + (BattleActionParams.QuickHealATBReplenishAmount * 100) + "%.";

        else if (id == BattleActionID.PoisonStrike)
            info = "Deals " + BattleActionParams.PoisonStrikeMod + " * Strength of physial damage to target foe and inflicts the Poison status effect.  Damage dealt is reduced by foe's Physical Defense.  Poison deals " + StatusEffectsParams.PoisonTickDamage + " damage to the affected foe after each of their turns and lasts 4 turns.";

        else if (id == BattleActionID.StunStrike)
            info = "Deals " + BattleActionParams.StunStrikeDamageMod + " * Strength of physial damage to target foe and reduces their Initiative by " + Mathf.Abs(BattleActionParams.StunStrikeInitReduction) + "% of the amount require to have a turn.  Damage dealt is reduced by foe's Physical Defense.";

        else if (id == BattleActionID.SilenceStrike)
            info = "Deals " + BattleActionParams.SilenceStrikeDamageMod + " * Strength of physial damage to target foe and inflicts the Silence status effect.  A character affected by the Silence status effect can not perform actions with a mana cost.  The Silence status effect lasts " + StatusEffectsParams.SilenceStrikeDuraction + " turn.";


        else if (id == BattleActionID.ManaBurn)
            info = "Deals " + BattleActionParams.ManaBurnManaDamageMod + " * Intelligence of magic damage to target foe's mana.  Damage dealt is reduced by foe's Magic Defense.";



        #endregion

        #region Status Effects

        else if (id == BattleActionID.Haste)
            info = "Applies the Haste status effect upon target ally.  Haste increases the targeted ally's Speed by " + (100f * StatusEffectsParams.HasteSpeedMod) + "% and lasts " + StatusEffectsParams.HasteDuration + " turns.";
        else if (id == BattleActionID.Slow)
            info = "Applies the Slow status effect upon target foe.  Slow decreases the targeted foe's Speed by " + (100f * StatusEffectsParams.SlowSpeedMod) + "% and lasts " + StatusEffectsParams.SlowDuration + " turns.";
        //info = "Applies the Slow status effect onto foe target.  Lasts " + StatusEffectsParams.SlowDuration + " of the target's turns.  The Slow status effect reduces the affected character's speed stat by " + (100f * StatusEffectsParams.SlowSpeedMod) + "%.";
        else if (id == BattleActionID.Faith)
            info = "Applies the Faith status effect upon target ally.  Faith increases the targeted ally's Intelligence by " + (100f * StatusEffectsParams.FaithPercentMod) + "% and lasts " + StatusEffectsParams.FaithDuration + " turns.";
        else if (id == BattleActionID.Brave)
            info = "Applies the Brave status effect upon target ally.  Brave increases the targeted ally's Strength by " + (100f * StatusEffectsParams.BravePercentMod) + "% and lasts " + StatusEffectsParams.BraveDuration + " turns.";
        else if (id == BattleActionID.Defaith)
            info = "Applies the Defaith status effect upon target foe.  Defaith decreases the targeted foe's Intelligence by " + (100f * StatusEffectsParams.DefaithPercentMod) + "% and lasts " + StatusEffectsParams.DefaithDuration + " turns.";
        //info = "Applies the Defaith status effect onto foe target.  Lasts " + StatusEffectsParams.DefaithDuration + " of the target's turns.  The Defaith status effect reduces the affected character's magic stat by " + (100f * Mathf.Abs(StatusEffectsParams.DefaithPercentMod)) + "%.";
        else if (id == BattleActionID.Debrave)
            info = "Applies the Debrave status effect upon target foe.  Debrave decreases the targeted foe's Strength by " + (100f * StatusEffectsParams.DebravePercentMod) + "% and lasts " + StatusEffectsParams.DebraveDuration + " turns.";
        //"Applies the Debrave status effect onto foe target.  Lasts " + StatusEffectsParams.DebraveDuration + " of the target's turns.  The Debrave status effect reduces the affected character's defense stat by " + (100f * Mathf.Abs(StatusEffectsParams.DebravePercentMod)) + "%.";

        else if (id == BattleActionID.AutoLife)
            info = "Upon being knocked out, a character affected by the Auto Life status effect is revived and fully healed.  The Auto Life status effect lasts " + StatusEffectsParams.AutoLifeDuration + " turns or until it revives the character affected by it.";
        // Auto Life == Applies the Auto Life status effect upon target ally.  Upon being knocked out, a character affected by the Auto Life status effect is revived and fully healed.
        else if (id == BattleActionID.Petrify)
            info = "Applies the Petrifying status effect upon target foe.  The Petrifying status effect lasts 3 turns, after which, the targeted foe will be affected by the Petrified status effect.  The Petrified status effect reduces the targeted foe's speed and initiative to 0.";
        //"Applies the Petrifying status effect upon target foe.  Petrifying lasts " + StatusEffectsParams.PetrificationTime + " turns, after which the foe has the Petrified status effect applied.  Petrified reduces speed and initiative to 0.";
        else if (id == BattleActionID.PoisonNova)
            info = "Applies the poison status effect upon all foes.  Poison deals " + StatusEffectsParams.PoisonTickDamage + " damage to the affected foe after each of their turns and lasts 4 turns.";

        else if (id == BattleActionID.Doom)
            info = "Applies the Doom status effect upon target foe.  The Doom status effect knocks out the affected character after " + StatusEffectsParams.DoomTime + " turns.";

        #endregion




        else if (id == BattleActionID.QuickCleanse)
            info = "Removes all negative status effects from target ally.  The character casting Quick Cleanse has their initiative replenished by " + (BattleActionParams.QuickCleanseATBReplenishAmount * 100) + "%.";

        else if (id == BattleActionID.QuickDispel)
            info = "Removes all positive status effects from target foe.  The character casting Quick Dispel has their initiative replenished by " + (BattleActionParams.QuickDispelATBReplenishAmount * 100) + "%.";

        else if (id == BattleActionID.Cleanse)
            info = "Removes all negative status effects from target ally.";

        else if (id == BattleActionID.Dispel)
            info = "Removes all positive status effects from target foe.";



        else if (id == BattleActionID.SilenceRemedy)
            info = "Removes the Silence status effects from target ally.  The character using Silence Remedy has their initiative replenished by " + (BattleActionParams.SilenceRemedyATBReplenish * 100) + "%.";
        else if (id == BattleActionID.PoisonRemedy)
            info = "Removes the Poisoned status effects from target ally.  The character using Poison Remedy has their initiative replenished by " + (BattleActionParams.PoisonRemedyATBReplenish * 100) + "%.";
        else if (id == BattleActionID.PetrifyRemedy)
            info = "Removes the Petrifying or Petrified status effects from target ally.";
        else if (id == BattleActionID.FullRemedy)
            info = "Removes all negative status effects from target ally.";





        else if (id == BattleActionID.Steal)
            info = "Removes the lowest value item from your opponent's item stash and adds it to your item stash.";


        else if (id == BattleActionID.CraftPotion)
            info = "Adds " + BattleActionParams.PotionCraftAmount + " Potions to your item stash.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftEther)
            info = "Creates " + BattleActionParams.EtherCraftAmount + " Ethers.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftElixer)
            info = "Creates " + BattleActionParams.ElixirCraftAmount + " Elixer.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftMegaElixer)
            info = "Creates " + BattleActionParams.MegaElixirCraftAmount + " Mega Elixer.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftRevive)
            info = "Creates " + BattleActionParams.CraftAmountRevive + " Revive.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftSilenceRemedy)
            info = "Creates " + BattleActionParams.CraftAmountSilenceRemedy + " Silence Remedy.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftPoisonRemedy)
            info = "Creates " + BattleActionParams.CraftAmountPoisonRemedy + " Poison Remedy.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftPetrifyRemedy)
            info = "Creates " + BattleActionParams.CraftAmountPetrifyRemedy + " Petrify Remedy.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";

        else if (id == BattleActionID.CraftFullRemedy)
            info = "Creates " + BattleActionParams.CraftAmountFullRemedy + " Full Remedy.  Costs " + CraftMaterialCost.GetMaterialCostForBattleAction(id) + " Craft Material.";





        if (info == "")
            info = "Battle Action Info Not Found!";


        int cost = BattleActionMPCost.GetMPCostForBattleAction(id);
        if (cost > 0)
            info = "Costs " + cost + " mana.\n" + info;


        return info;

    }

    private static string GetDamageText(bool isDamage, bool isMagic, bool isAll, float mod)//, string type)
    {
        string info = "";

        if (isDamage)
        {
            if (isMagic)
            {
                if (isAll)
                    info = "Deals " + mod + " * Intelligence of magic damage to all foes.  Damage dealt is reduced by each foe's Magic Defense.";
                else
                    info = "Deals " + mod + " * Intelligence of magic damage to target foe.  Damage dealt is reduced by foe's Magic Defense.";
                //"Deals magic damage to target foe.  Where damage = " + mod + " * [Wisdom]";
            }
            else
            {
                if (isAll)
                    info = "Deals " + mod + " * Strength of physical damage to all foes.  Damage dealt is reduced by each foe's Physical Defense.";
                else
                    info = "Deals " + mod + " * Strength of physial damage to target foe.  Damage dealt is reduced by foe's Physical Defense.";
                // info = "Deals physical damage to all foes.  Where damage = " + mod + " * [Strength]";
                // else
                //     info = "Deals physical damage to target foe.  Where damage = " + mod + " * [Strength]";
            }
        }
        else
        {
            if (isAll)
                info = "Heals " + mod + " * Intelligence health of each ally character.";
            else
                info = "Heals " + mod + " * Intelligence health of target ally character.";
            //info = "Heals a friendly target.  Where heal = " + mod + " * [Wisdom].";
        }

        return info;
    }

}

public static class PassiveAbilityInfo
{
    public static string GetInfoForID(int id)
    {
        string info = "not found!!!";

        if (id == PassiveAbilityID.Cover)
            info = "If an ally Wizard or Cleric would be hit by a physical attack, a character with the Cover passive ability will intercept and take the hit.  In the event that there are two ally characters with the Cover passive ability, the character with the greatest amount of Health will take the hit.";
        else if (id == PassiveAbilityID.ItemJockey)
            info = "After using an item, initiative bar is filled 40%.";
        else if (id == PassiveAbilityID.StealthAttack)
            info = " A character with the Sneak Attack passive ability will not have their physical attacks intercepted by the Cover passive ability.";
        else if (id == PassiveAbilityID.Larceny)
            info = "If a character with the Larceny passive ability steal an item of " + PassiveEffectParams.LarcenyProcIfUnder + " or less value, they also steal a second item.";
        else if (id == PassiveAbilityID.ExtraDmgOnLowHealth)
            info = "If a character has 50% or lower life it deals +50% extra damage.";
        else if (id == PassiveAbilityID.ExtraDamageVsPoisoned)
            info = "Deals +25% extra damage when hitting a poisoned character.";

        return info;
    }
}

public static partial class StatusEffectInfo
{
    public static string GetInfoForID(int id)
    {
        string info = "not found!!!";

        if (id == StatusEffectID.Poison)
            info = "txt for poison";
        else if (id == StatusEffectID.AutoLife)
            info = "txt for AutoLife";
        else if (id == StatusEffectID.Haste)
            info = "txt for Haste";
        else if (id == StatusEffectID.Slow)
            info = "txt for Slow";
        else if (id == StatusEffectID.Faith)
            info = "txt for Faith";
        else if (id == StatusEffectID.Brave)
            info = "txt for Brave";
        else if (id == StatusEffectID.Debrave)
            info = "txt for Debrave";
        else if (id == StatusEffectID.Defaith)
            info = "txt for Defaith";
        else if (id == StatusEffectID.Silence)
            info = "txt for Silence";
        else if (id == StatusEffectID.Petrifying)
            info = "txt for Petrifying";
        else if (id == StatusEffectID.Doom)
            info = "txt for Doom";

        return info;
    }

}

public static class ItemEffectInfo
{
    public static string GetInfoForID(int id)
    {
        string info = "not found!!!";

        if (id == ItemID.Potion)
            info = "txt for Potion";
        else if (id == ItemID.Ether)
            info = "txt for Ether";
        else if (id == ItemID.Elixir)
            info = "txt for Elixir";
        else if (id == ItemID.MegaElixir)
            info = "txt for MegaElixir";
        else if (id == ItemID.Revive)
            info = "txt for Revive";
        else if (id == ItemID.SilenceRemedy)
            info = "txt for SilenceRemedy";
        else if (id == ItemID.PoisonRemedy)
            info = "txt for PoisonRemedy";
        else if (id == ItemID.PetrifyRemedy)
            info = "txt for PetrifyRemedy";
        else if (id == ItemID.FullRemedy)
            info = "txt for FullRemedy";
        else if (id == ItemID.CraftMaterial)
            info = "txt for CraftMaterial";

        return info;
    }

}

